<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [The uuutils package](#the-uuutils-package)
  - [Installing](#installing)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# The uuutils package

This package provides various utility functions, largely written by me while
still working at UMC Utrecht, hence the name.

## Installing


```
# If the remotes (or equivalently, the devools) installer package is not installed, 
# install it as:

> install.packages("remotes") ## only needed once. 

#  Now install the uuutils package as:

> remotes::install_bitbucket("princessmaximacenter/uuutils")

# To start using it do

> library(uuutils)

```

The 'home' of `uuutils` is https://bitbucket.org/princessmaximacenter/uuutils.git

