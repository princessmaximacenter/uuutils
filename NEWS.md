# v1.60

* added jaccard_pct as another element to the venn.summary output

# v1.59

fixed bug in pairs.with.correlation; changed hooks a bit.

# v1.58

various

# v1.57

* .gitversion() + various

# v1.56

* functions remove.nulls, %||%, %!%, %_%, reorder.column. 
  (Also fixed NEWS.md)

# v1.55

* functions undebug.all, debugged.functions
* various tweaks

# v1.54 

* ls.bytype(), ls.funcs(), ls.nonfuncs(), ls.dataframes, ls.lists()

* many different things in the meantime, check git log. Latest changes:
  added chunk, made .is.good.set more permissive.

# v.146 - 153: ???

# v1.45  

* added read.json, improved getversion

# v1.44 

* better getversion()

#  v1.43 

* allowing additional arguments to splinefun in auc

# v1.42 

* added expand.range; docutweaks on draw.color.scale; colorize now deals
  with NA's (which get their own color using new na.col argument)
  color.scheme is now defunct.

# v1.41 

* added logit & invlogit (straight from arm package version 1.9-3)

# v1.40 
 
* avoiding warnings 'Rowv is FALSE, while dendrogram is `both', work
  around for gplots::heatmap.2 bug, and now actually returning the
  heatmap.2 result.

# v1.39 

* added delete.spaces argument to reads.sets.

# v1.38 

* added read.sets. Started to add some docu using roxygen, but far from
  complete.

# v1.37 

* added plotting of marginals and confidence lines to plot.cor

# v1.36 

* renamed mode to estimate.mode, and added docu. 

# v1.35 

* documentation on hypergeo; changed author email

# v1.34 

* added add.libpath, setenv.from.envstring, cuzick test; fixed a few
  bugs and added docu. Added 'test.depletion' arugment to hypergeo.

# v1.32 

* added zeroterminate.islands.

# v1.31 

* added correlated.data(), misc. small bug fixes

# v1.30 

* Name Change from phutils to uuutils. Also got rid of parseArgs, which
  is now in its own separate package parseArgs.

# ? 

* obsoleted is.sorted (use !unsorted, which is faster)

# v 1.29 

* brought phutils-package.Rd up to date

# v1.28 

* hopach replaces labels with 1:nrow (^%$#@!), now put back in cor.dist
  and cosangle.dist

# v1.27 

* added functions hdist2dist, cor.dist, cosangle.dist to fix weird
  problem with simple.heatmap.2. hopach and gplots now a dependency;
  calls now fullly::qualified

# v1.26 

* Added function simple.heatmap.2 with sensible defaults, understandble
  options and the possibility to do Pearson or Cosine correlation
  clustering. 

# v1.25 

* renamed color.scale to draw.color.scale to avoid conflict with plotrix

# v1.24  

* added function invent.data(). plot.cor() can now take x (2-col) without y.

# v1.22  

* in figures(), added imaginary sizes to subfigures for making them square

# v1.21 

* fixes to figures() so it also builds on windows. 

# v1.20 

* raising error on simulate.p.value=TRUE in g.test; extensive Fixes on
  the examples so they pass the R CMD build check.

#  v1.19 

* added args to pairs.with.correlations to control text sizes

# v1.18  

* figures function and docu fully functional; version 

# v1.17

* dot.product now more general; added zerofy; misc. small things

# v1.16

* added docu  on hypergeo

# v1.15 

* added mode()

# v1.14 various small changes such as robust.z, (p)reallocate.data.frame.

# v1.13 

* panels() now prints the remaining margins (as a warning())

# v1.12 

* added panels function + docu, and this ChangeLog.

