\name{rotation.matrix.3d}
\alias{rotation.matrix.3d}

\title{
Produce 3D rotation matrix.
}
\description{
  Returns 3x3 matrix for rotation of angle radians about axis v
}

\usage{
  rotation.matrix.3d(v, angle)
}

\arguments{
  \item{v}{
    3D unit vector about which the rotation is to take place
}
  \item{angle}{
    The angle to rotate around
  }
}

\value{
  A 3D matrix.
}

\author{
  Philip Lijnzaad <plijnzaad@gmail.com>
}
\note{
  I assume the coordinates a row vectors (so for n vectors the set of
  coordinates is an n x 3 matrix), and the rotation is carried 
  out by right-multiplying with the rotation matrix.
}


\seealso{
 \code{\link{kabsch}}
}


\examples{

  v <- runif(3)
  v <- v/eucl.norm(v)
  m <- rotation.matrix.3d(v, angle=2*pi*runif(1))

}
