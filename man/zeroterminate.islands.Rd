\name{zeroterminate.islands}
\alias{zeroterminate.islands}

\title{
  Surround 'islands' of closely spaced x-values with extra zero values.
}

\description{
  When (linearly) interpolating non-equidistant data, the predicted y-values
  spanning large x-gaps would be between the y-values of the data points
  on either side of the gap. In most cases these y-values should however
  be zero. This function achieves that by surrounding each island with a
  zero (or other values) on both sides.
}

\usage{
  zeroterminate.islands(x, y,
                       wanted.dist = function(dists){median(dists)},
                       max.dist = function(dists){3*median(dists)},
                       value = 0)
}

\arguments{
  \item{x}{
    The x values. Must be sorted.
  }
  \item{y}{
    The y values.
  }
  \item{wanted.dist}{
    The distance to extend into a gap on either side of an island.
    If this argument is a function, it is called (with the vector of
    x-distances as argument) to calculate it, otherwise it is used as is.
  }
  \item{max.dist}{
    The distance above which islands will get an extra zero value on
    either side. If this argument is a function, it is called (with the
    vector of x-distances as argument) to calculate it, otherwise it is
    used as is.

    Note that \code{max.dist} must be \code{> 2*wanted.dist},
    otherwise the ordering of newly added zero-termini may be reversed.

  }
  \item{value}{
    The value to surround the islands with.
  }
}

\value{
  \item{x}{The new x values (have now an 2*(number of gaps) additional values)}
  \item{y}{The new y values (have now an 2*(number of gaps) additional
    values, all of them equal to \code{value} }
  \item{gap.starts, gap.ends}{Location the gaps in the original \code{x}}
}

\author{
  Philip Lijnzaad <plijnzaad@gmail.com>
}

\note{
  This function is meant for doing linear interpolations. Ordinary
 spline functions on the resulting data, they will grossly overshoot.
}

\seealso{\code{\link{approxfun}}}

\examples{
  x <- cumsum(sample(c(1,1,1,2,2,3,30), size=20, replace=TRUE))
  y <- runif(length(x))
  plot(x,y, ylim=c(0,1),col="red",
       main="green line gives better interpolation",
       xlab="Non-equidistant x-values",
       ylab="Random data")
  f <- approxfun(x,y)
  xout <- sort(c(x, seq(min(x), max(x), length.out=100)))
  lines(xout, f(xout), col="red", lty=2)
  l <- zeroterminate.islands(x,y)
  points(l$x, l$y, cex=2, col="DarkGreen")
  f2 <- approxfun(l$x,l$y)
  xout2 <- sort(c(l$x, seq(min(l$x), max(l$x), length.out=100)))
  lines(xout2, f2(xout2), col="DarkGreen", lwd=2)

}

\keyword{misc}

