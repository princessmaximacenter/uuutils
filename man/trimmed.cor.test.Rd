\name{trimmed.cor.test}
\alias{trimmed.cor.test}

\title{
   Quantile-trim the data, then perform correlation test on it.
}
\description{
  To reduce influence of outliers, the top and bottom 5
  percentile are trimmed off the data. This function does that before
  running  a correlation test on it.
}

\usage{
  trimmed.cor.test(x, y, fraction = 0.05, ...)
}

\arguments{
  \item{x,y}{ Data to test the correlation of.
}

\item{fraction}{
   What fraction to take off the ends. If length is 1, that
   quantile is taken off both ends; if length is 2, \code{fraction[1]}
   is taken off the left, and \code{fraction[2]} is taken off the right
   tail of the data
}
  \item{\dots}{
   Passed to \code{\link{cor}}      
}
}
\details{
  The argument 'use="pairwise.complete"' is passed to \code{cor.test}
}

\value{
  See \code{\link{cor.test}}
}

\author{
  Philip Lijnzaad <plijnzaad@gmail.com>
}

\seealso{
  \code{\link{trim}}, \code{\link{trimmed.cor}}, \code{\link{cor.test}}
}

\examples{
 x <- rnorm(100)
 y <- x+0.5*rcauchy(100)
 cor.test(x,y)
 trimmed.cor.test(x,y)
}
\keyword{misc}
