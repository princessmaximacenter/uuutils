\name{estimate.mode}
\alias{estimate.mode}
\title{
  Function to determine the mode of a population. 
}
\description{

  This function returns the 'peak of the
  distribution' by determining its density using \code{\link{density}}.
  (confusingly, the R function \code{\link{mode}} 
  shows the 'storage mode' of an object, not the 'peak of the
  distribution')
}

\usage{
  estimate.mode(x, from = -Inf, to = Inf, ...)
}

\arguments{
  \item{x}{ numeric vector of measurements, or its \code{\link{density}}
}

\item{from,to}{ Mode is searched between these two limits. If \code{x}
not a density, the mode is determined first, and limits (if any) are
applied afterwards.
}
  \item{\dots}{
    All other arguments are passed to \code{\link{density}}
  }
}

\details{
  This mode function calculates the empirical mode (i.e., 'peak') of a
  distribution by finding the peak of its density. 
}

\value{
  The mode.
}

\references{
  Rasmus Bååth, http://stackoverflow.com/a/13874750 and
  AleRuete, http://stackoverflow.com/a/18763500.
}

\author{
 Philip Lijnzaad <plijnzaad@gmail.com>
}

\note{
  For more sophisticated mode finding, see package \code{modeest}.
}

\seealso{ 

  \code{\link{is}}, \code{\link{typeof}}, \code{\link{class}},
  \code{\link{density}}
  
}

\examples{

x <- rnorm(100, mean=runif(n=1, min=-10,max=10), sd=runif(n=1, min=1, max=10))
plot(density(x))
abline(v=estimate.mode(x), col='red')

}

\keyword{univar}
